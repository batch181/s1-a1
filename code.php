<?php
//get full address activity
$specAdd = "123 main st.";
$province = "Rizal";
$city = "Cubao";
$country = "Philippines";

function getFullAddress($specAdd, $province, $city, $country){
    return "$specAdd, $province, $city, $country";
}


//get grades activity
function getLetterGrade($grade){
	if($grade <= 75){
		return "You got D, Depression detected!";
	}else if($grade >= 75 && $grade <= 76){
		return "You Got C-.";
	}else if($grade >= 77 && $grade <= 79){
		return "You Got C.";
	}else if($grade >= 80 && $grade <= 82){
		return "You Got C+.";	
	}else if($grade >= 83 && $grade <= 85){
		return "You Got B-.";
	}else if($grade >= 86 && $grade <= 88){
        return "You Got B.";
    }else if($grade >= 89 && $grade <= 91){
        return "You Got B+!";
    }else if($grade >= 92 && $grade <= 94){
        return "You Got A-! Good job! :)";
    }else if($grade >= 95 && $grade <= 97){
        return "Congratulations! You got A! :)";
    }else if($grade >=98 && $grade <=100){
        return "SANAOL, You got A+!";
    }else{
        return "Please select a valid input!";
    }
}